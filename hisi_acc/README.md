# Kunpeng Acceleration Engine

- [Introduction](#introduction)
- [License](#license)
- [Requirements](#requirements)
- [Installation Instructions](#installation-instructions)
    - [Building OpenSSL](#building-openssl)
    - [Cloning and Building Kunpeng Acceleration Engine](#Cloning and Building Kunpeng Acceleration Engine)
    - [Testing Kunpeng Accelerator Engine](#Testing Kunpeng Accelerator Engine)
- [Examples](#Examples)
- [Troubleshooting](#troubleshooting)
- [Loading Engines by Setting the OpenSSL Configuration File](#Loading Engines by Setting the OpenSSL Configuration File)+
- [More Information](#more information)
- [Copyright](#Copyright)

## Introduction

Kunpeng Acceleration Engine is a new technology within Hisilicon Kunpeng 920 processors which 

provides a hardware-enabled foundation for security, authentication, and compression. It significantly increases the performance across cloud, networking, big data, and storage applications and  platforms.

Kunpeng Acceleration Engine includes symmetric encryption, asymmetric encryption, digital signatures, and RSA for accelerating SSL/TLS application, which makes processors more efficient and reduces hardware costs. By accelerating SSL/TLS with Kunpeng Acceleration Engine, you can:

- Support higher-performance secured tunnels and a greater number of authenticated clients
- Have higher-performance encrypted traffic throughout a secured network
- Accelerate compute-intense symmetric and asymmetric cryptography
- Have greater platform application efficiency
- Have higher-performance compression and decompression
- Maximize CPU utilization

So far, the algorithms supported by Kunpeng Acceleration Engine are:

- Asymmetric encryption algorithm:  RSA Support Key Sizes 1024/2048/3072/4096
- Digest algorithm: SM3
- Block cipher algorithm: SM4 Support CTR/XTS/CBC
- Compression and decompression algorithm: gzip/zlib

## License

It is licensed under the [APACHE LICENSE, VERSION 2.0](https://www.apache.org/licenses/LICENSE-2.0 ). For more information, see the LICENSE file. 

## Requirements

* CPU: Kunpeng 920 
* Operating System: 
  * CentOS 7.6  4.14.0-115.el7a.0.1.aarch64 version
  * SuSE 15.1 4.12.14-195-default arch64 version
* OpenSSL 1.1.1a  or later OpenSSL

## Installation Instructions

### Building OpenSSL

Clone OpenSSL from Github at the following location:

    git clone https://github.com/openssl/openssl.git

You are advised to check out and build the OpenSSL 1.1.1a git tag specified in the release notes.
Versions of OpenSSL before OpenSSL 1.1.0 are not supported.

Note: You are not advised to install the accelerated version of OpenSSL as your default system library. Otherwise, acceleration may be used unexpectedly by other applications on the system, resulting in undesired/unsupported behavior. The `--prefix` can be used with the `./config` command to specify the location that `make install` will copy files to. Please see the OpenSSL INSTALL file for full details on usage of the `--prefix` option.

By default, we usually install OpenSSL as follows:

    ./config
    make
    make install
Alternatively, you can specify the install path by adding it to the environment variable `LD_LIBRARY_PATH` by the command:

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

### Cloning and Building Kunpeng Acceleration Engine

Clone the Github repository containing the Kunpeng Accelerator Engine:

    git clone https://github.com/huawei/acceleration

Note: To build the Kunpeng Accelerator Engine, install the `kernel-devel` package first.

The code contains four parts, accelerator driver, accelerator library, accelerator OpenSSL engine and the patch for zlib. 

Firstly, install the accelerator driver:

```
cd hisi_acc/drivers/kmodules
make
make install
```
Secondly, install the accelerator library:

```
cd hisi_acc/drivers/warpdriver 
sh autogen.sh 
./configure 
make 
make install
```

Then, install the accelerator OpenSSL engine:

```
cd hisi_acc/kunpeng_engine 
chmod +x configure 
./configure 
make  
make install

```

Note: The `--prefix` can be used with the `./configure` command to specify the location that  `make install` will copy files to. The default installation path for the accelerator engine is `/usr/local/lib/openssl-1.1`. You are advised to install the accelerator OpenSSL engine by default as the OpenSSL; otherwise you need to export the environment variable `OPENSSL_ENGINES`  as follows (assuming that the path is the one specified by`--prefix`):

```
export OPENSSL_ENGINES=/usr/local/ssl/lib/engines-1.1
```

Install the zlib acceleration library:

```
cd hisi_acc/open_source
tar –zxf zlib-1.2.11.tar.gz 
cd zlib-1.2.11
patch -Np1 < ../kunpeng_zlib_1.0.8.patch
./configure
make clean && make 
make install
```

Note: Please download the zlib-1.2.11 version source package, and put it in the hisi_acc/open_source/zlib/ directory, then compile the zlib as the command above.

### Testing Kunpeng Accelerator Engine

Run the following command to check when the Kunpeng Accelerator Engine is loaded correctly:

```
cd /path/to/openssl/apps
./openssl  genrsa -out test.key -engine kae 2048 
./openssl  rsa -in test.key -pubout -out test_pub.key -engine kae 
./openssl  rsautl -encrypt -in rsa_test -inkey test_pub.key -pubin -out rsa_test.en -engine kae 
./openssl  rsautl -decrypt -in rsa_test.en -inkey test.key -out rsa_test.de -engine kae
```

```
./openssl enc -sm4-cbc -a -in sm4_test -out sm4_test.en -pass pass:123456 -engine kae 
./openssl enc -sm4-cbc -a -in sm4_test -out sm4_test.en -pass pass:123456 -p -engine kae 
./openssl enc -sm4-cbc -d -a -in sm4_test.en -out sm4_test.de -pass pass:123456 -engine kae 
./openssl sm3 -out sm3_out -engine kae sm3_test
```

```
cd hisi_acc/open_source/zlib/zlib-1.2.11
make check
```

## Examples

Here is an example to show you how to use the Kunpeng Accelerator Engine. 

```
#include <stdio.h> 
#include <stdlib.h> 
 
/* OpenSSL headers */ 
#include <openssl/bio.h> 
#include <openssl/ssl.h> 
#include <openssl/err.h> 
#include <openssl/engine.h> 
  
int main(int argc, char **argv) 
{ 
    /* Initializing OpenSSL */ 
    SSL_load_error_strings(); 
    ERR_load_BIO_strings(); 
    OpenSSL_add_all_algorithms(); 
     
    /*You can use ENGINE_by_id Function to get the handle of the Kunpeng Accelerator Engine*/ 
    ENGINE *e = ENGINE_by_id("kae");
    ENGINE_init(e); 
    ENGINE_set_default(e, ENGINE_METHOD_ALL); 
     
    /*The user code To Do */ 
     
    ENGINE_finish(e); 
    ENGINE_free(e); 
    ENGINE_cleanup(); 
}

```

## Troubleshooting

The most likely failure point is that the Kunpeng Accelerator Engine is not loaded successfully. If this occurs:

   1. Check that the accelerator driver has been loaded successfully by running the `lsmod` command. 

      `uacce.ko, qm.ko, sgl.ko, hisi_sec2.ko, hisi_hpre.ko, hisi_zip.ko` should be in the list. 

   2. Check that the paths have been set correctly so that the `libkae.so` engine file can be copied to the correct location.

   3. Check that the installation path has been correctly added to the environment variable `OPENSSL_ENGINES` and exported to the shell by running the `export` command.

## Loading Engines by Setting the OpenSSL Configuration File 

By setting up the OpenSSL configuration file, you can also initialize the Kunpeng Accelerator Engines for your OpenSSL application. For further details on using the `openssl.cnf` file, see the OpenSSL online documentation at:

<https://www.openssl.org/docs/man1.1.0/apps/config.html>

Here is an example to show you how to set up the  `openssl.cnf`  file to load engines. Add the following statements to the global section (assuming that the path is the one that accelerator OpenSSL engine installed):

    openssl_conf = openssl_engine_init
    
    [ openssl_engine_init ]
    engines = engine_section
    
    [ engine_section ]
    kae = kae_section
    
    [ kae_section ]
    engine_id = kae
    dynamic_path = /usr/local/ssl/lib/engines-1.1/kae.so
    default_algorithms = ALL

## More Information

For further assistance, contact Huawei Support at:

<https://support.huawei.com>


## Copyright

Copyright © 2018 Huawei Corporation. All rights reserved.